package mk.ukim.finki.emt2019.ordermanagement.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * @author Riste Stojanov
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class EmailTest {


    @Autowired
    JavaMailSender mailSender;

    @Test
    public void sendSimpleMail() {
        String userEmail = "riste.stojanov@finki.ukim.mk";
        String subject = "Activate your account";


        SimpleMailMessage email = new SimpleMailMessage();
        email.setFrom("jdapps2016@gmail.com");
        email.setTo(userEmail);
        email.setSubject(subject);
        email.setText("Activate your account using the following url" + " \n" + "http://localhost:8080");
        mailSender.send(email);
    }


    @Test
    public void sendHtmlMail() throws MessagingException {
        String userEmail = "riste.stojanov@finki.ukim.mk";
        String subject = "Activate your account";

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);


        helper.setFrom("jdapps2016@gmail.com");
        helper.setTo(userEmail);
        helper.setSubject(subject);
        helper.setText("<html>" +
                "<body>" +
                "<h4>Activate your account</h4>" +
                "<div>Activate your account using the <a href=\"http://localhost:8080\">following url</a></div>" +
                "</body>" +
                "</html>", true);

        mailSender.send(message);
    }


}
