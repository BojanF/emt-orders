package mk.ukim.finki.emt2019.ordermanagement.repository;

import mk.ukim.finki.emt2019.ordermanagement.model.*;
import mk.ukim.finki.emt2019.ordermanagement.model.projections.ShoppingCartExpiryProjection;
import mk.ukim.finki.emt2019.ordermanagement.model.views.CartTotalAndItems;
import mk.ukim.finki.emt2019.ordermanagement.repository.jpa.AccountRepository;
import mk.ukim.finki.emt2019.ordermanagement.repository.jpa.CartStatusRepository;
import mk.ukim.finki.emt2019.ordermanagement.repository.jpa.ProductRepository;
import mk.ukim.finki.emt2019.ordermanagement.repository.jpa.ShoppingCartRepository;
import mk.ukim.finki.emt2019.ordermanagement.service.view_model.CartTotalAndNumberOfItems;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Riste Stojanov
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
//@ActiveProfiles("testmem")
public class ShoppingCartRepositoryTest {

    @Autowired
    private ShoppingCartRepository cartRepository;

    @Autowired
    private ProductRepository productRepository;


    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CartStatusRepository statusRepository;

    @Autowired
    private TxHelper txHelper;

    @PersistenceContext
    private EntityManager em;


    private Product p1, p2, p3;
    private Account a1;

    @Before
    public void init() {
        p1 = new Product();
        p1.productId = 1L;
        p1.displayName = "p1";
        p1.price = 10D;
        p1.stockQuantity = QuantityFactory.of(10D, Unit.PEACES);

        p2 = new Product();
        p2.productId = 2L;
        p2.displayName = "p2";
        p2.price = 20D;
        p2.stockQuantity = QuantityFactory.of(20D, Unit.KG);


        p3 = new Product();
        p3.productId = 3L;
        p3.displayName = "p3";
        p3.price = 30D;
        p3.stockQuantity = QuantityFactory.of(30D, Unit.GB);

        p1 = productRepository.save(p1);
        p2 = productRepository.save(p2);
        p3 = productRepository.save(p3);

        a1 = new Account();
        a1.clientName = "Peter";
        a1.accountNumber = UUID.randomUUID().toString();
        a1.totalBalance = 800D;
        a1 = accountRepository.save(a1);
    }

    @Test
    public void testCartCreate() {
        ShoppingCart cart = ShoppingCart.createWithExpiryInHours(1L);

        OrderItem orderItem = OrderItem.createWithExpiryInHours(1L, p1,
                QuantityFactory.of(2D, p1.stockQuantity.unit));
        cart.addItem(orderItem);

        cart = this.cartRepository.saveAndFlush(cart);

        cart.addItem(OrderItem.createWithExpiryInHours(1L, p2,
                QuantityFactory.of(2D, p2.stockQuantity.unit)));

        cart = this.cartRepository.saveAndFlush(cart);

        cart.changeItemQuantity(1L, QuantityFactory.of(5D, p1.stockQuantity.unit));

        cart.account = a1;

        cart = this.cartRepository.saveAndFlush(cart);

        em.clear();

        cart = txHelper.readCartInTx(cart.cartId);

        em.clear();

        cart = txHelper.fetchCartInTx(cart.cartId);

        em.clear();

        cart = txHelper.fetchWithAttributesCartInTx(cart.cartId);

        List<ShoppingCartExpiryProjection> test = this.cartRepository.getAllShoppingCartExpiryProjections();

        test.stream().forEach(i -> System.err.println("CartID: " + i.getCartId() + " Expiry Time: " + i.getExpiryTime()));

        Optional<CartTotalAndNumberOfItems> status = this.cartRepository.getCartStatus(cart.cartId);

        Optional<CartTotalAndItems> viewStatus = this.statusRepository.findById(cart.cartId);

        List<ShoppingCartExpiryProjection> expiryProjectionList = this.cartRepository.findAllBy(ShoppingCartExpiryProjection.class);


        Specification<ShoppingCart> spec = createSpecification(a1.accountNumber, null);

        List<ShoppingCart> specifiedList = this.cartRepository.findAll(spec);


        Specification<ShoppingCart> spec1 = createSpecification(a1.accountNumber, cart.cartId);

        specifiedList = this.cartRepository.findAll(spec1);
    }

    private Specification<ShoppingCart> createSpecification(String accountId,
                                                            Long cartId) {

        Specification spec = Specification.where(null);


        if (accountId != null)
            spec = spec.and((root, query, cb) -> cb.equal(root.get("account").get("accountNumber"), accountId));

        if (cartId != null)
            spec = spec.and((root, query, cb) -> cb.equal(root.get("cartId"), cartId));


        return spec;
    }


}
