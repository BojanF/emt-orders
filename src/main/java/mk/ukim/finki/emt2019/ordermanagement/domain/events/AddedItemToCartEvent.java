package mk.ukim.finki.emt2019.ordermanagement.domain.events;

import mk.ukim.finki.emt2019.ordermanagement.model.OrderItem;

/**
 * @author Riste Stojanov
 */
public class AddedItemToCartEvent {

    public String cartId;

    public OrderItem item;
    public Double productPrice;

    public AddedItemToCartEvent(String cartId, OrderItem item, Double productPrice ) {
        this.cartId = cartId;
        this.item = item;
    }
}
