package mk.ukim.finki.emt2019.ordermanagement.init;

import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;

/**
 * Created by ristes on 3/2/16.
 */
@Component
public class BuildSearchIndex {

    private final EntityManager entityManager;
    private final PlatformTransactionManager txManager;
    public Logger logger = LoggerFactory.getLogger(BuildSearchIndex.class);

    public BuildSearchIndex(EntityManager entityManager, PlatformTransactionManager txManager) {
        this.entityManager = entityManager;
        this.txManager = txManager;
    }

    @PostConstruct
    public void init() {

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        // explicitly setting the transaction name is something that can only be done programmatically
        def.setName("index_init");
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {


            FullTextEntityManager fullTextEntityManager =
                    Search.getFullTextEntityManager(entityManager);
            fullTextEntityManager.createIndexer().startAndWait();
            txManager.commit(status);
            logger.debug("Started!");
        } catch (InterruptedException e) {
            logger.warn(
                    "An error occurred trying to build the serach index: {}", e.toString(),
                    e);
        } catch (Exception e) {
            txManager.rollback(status);
        }
    }


}
