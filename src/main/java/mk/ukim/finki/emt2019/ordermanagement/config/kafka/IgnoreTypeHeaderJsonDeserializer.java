package mk.ukim.finki.emt2019.ordermanagement.config.kafka;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.header.Headers;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.util.Assert;

/**
 * @author Riste Stojanov
 */
public class IgnoreTypeHeaderJsonDeserializer extends JsonDeserializer {

    public IgnoreTypeHeaderJsonDeserializer() {
    }

    public IgnoreTypeHeaderJsonDeserializer(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    public IgnoreTypeHeaderJsonDeserializer(Class targetType) {
        super(targetType);
    }

    public IgnoreTypeHeaderJsonDeserializer(Class targetType, ObjectMapper objectMapper) {
        super(targetType, objectMapper,true);
        this.objectMapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, false);
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Override
    public Object deserialize(String topic, Headers headers, byte[] data) {
        Assert.state(this.targetType != null, "No type information in headers and no default type provided");
        return deserialize(topic, data);
    }
}
