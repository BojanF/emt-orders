package mk.ukim.finki.emt2019.ordermanagement.config.security;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * @author Riste Stojanov
 */
@Component
public class DummyUserDetails implements UserDetailsService {

    private final PasswordEncoder encoder;

    public DummyUserDetails(PasswordEncoder encoder) {
        this.encoder = encoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        return new org.springframework.security.core.userdetails.User(
                username,
                encoder.encode(username + "123"),
                Stream.of(new SimpleGrantedAuthority("ROLE_USER"))
                        .collect(toList())
        );
    }
}
