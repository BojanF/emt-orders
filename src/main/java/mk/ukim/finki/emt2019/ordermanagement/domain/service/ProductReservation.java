package mk.ukim.finki.emt2019.ordermanagement.domain.service;

import mk.ukim.finki.emt2019.ordermanagement.domain.events.AddedItemToCartEvent;
import mk.ukim.finki.emt2019.ordermanagement.domain.events.ItemRemovedFromCartEvent;
import mk.ukim.finki.emt2019.ordermanagement.model.Quantity;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Riste Stojanov
 */
@Component
public class ProductReservation {

    Map<Long, Quantity> productReservations = new HashMap<>();
    Map<Long, Quantity> productStock = new HashMap<>();

    @EventHandler
    public void handle(AddedItemToCartEvent event) {
        Quantity newQuantity = event.item.quantity.add(
                productReservations.get(event.item.productId)
        );
        this.productReservations.put(event.item.productId, newQuantity);
    }

    @EventHandler
    public void handle(ItemRemovedFromCartEvent event) {

        Quantity newQuantity = event.item.quantity.add(
                productReservations.get(event.item.productId)
        );
        this.productReservations.put(event.item.productId, newQuantity);
    }

    public Quantity getReservedQuantityForProduct(Long productId) {
        return this.productReservations.get(productId);
    }
}
