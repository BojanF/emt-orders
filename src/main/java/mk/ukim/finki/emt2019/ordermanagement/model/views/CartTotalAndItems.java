package mk.ukim.finki.emt2019.ordermanagement.model.views;

import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Riste Stojanov
 */
@Entity
@Table(name = "cart_items_and_total")
@Immutable
public class CartTotalAndItems {

    @Id
    public Long cartId;

    public Integer itemsCount;

    public Double totalPrice;
}
