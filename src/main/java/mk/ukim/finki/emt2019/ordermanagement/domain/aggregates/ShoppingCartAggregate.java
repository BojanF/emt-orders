package mk.ukim.finki.emt2019.ordermanagement.domain.aggregates;

import mk.ukim.finki.emt2019.ordermanagement.domain.commands.*;
import mk.ukim.finki.emt2019.ordermanagement.domain.events.*;
import mk.ukim.finki.emt2019.ordermanagement.model.Account;
import mk.ukim.finki.emt2019.ordermanagement.model.OrderItem;
import mk.ukim.finki.emt2019.ordermanagement.model.Quantity;
import mk.ukim.finki.emt2019.ordermanagement.model.exceptions.NoProductInCartException;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.modelling.command.AggregateMember;
import org.axonframework.spring.stereotype.Aggregate;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Riste Stojanov
 */
@Aggregate(snapshotTriggerDefinition = "eventCountSnapshotTriggerDefinition")
public class ShoppingCartAggregate {

    @AggregateIdentifier
    public String cartId;


    public LocalDateTime expiryTime;

    public Account account;

    public Double totalPrice = 0D;

    public Integer itemsCount = 0;

    public Boolean overfilled = false;

    /*
        Information about orphanRemoval: https://www.objectdb.com/java/jpa/persistence/delete#Orphan_Removal_
     */
    @AggregateMember
    public List<OrderItem> items = new ArrayList<>();

    public ShoppingCartAggregate() {

    }

    @CommandHandler
    public ShoppingCartAggregate(AddItemToNewCartCommand command) {
        // todo: validate if this command can be executed
        AggregateLifecycle.apply(new CartCreatedEvent(command.cartId,
                LocalDateTime.now().plusHours(command.cartExpiryInHours),
                command.product,
                command.quantity));
        AggregateLifecycle.apply(new AddedItemToCartEvent(
                        command.cartId,
                        OrderItem.createWithExpiryInHours(command.cartExpiryInHours,
                                command.product,
                                command.quantity),
                        command.product.price
                )
        );

    }

    @CommandHandler
    public ShoppingCartAggregate(AddItemToNewLinkedCartCommand command) {

    }

    public static ShoppingCartAggregate createWithExpiryInHours(Long expiresAfterHours) {
        ShoppingCartAggregate cart = new ShoppingCartAggregate();
        cart.expiryTime = LocalDateTime.now().plusHours(expiresAfterHours);
        return cart;
    }

    @CommandHandler
    public void process(AddItemToCartCommand command) {

    }

    @CommandHandler
    public void process(ChangeItemQuantityCommand command) {

    }

    @CommandHandler
    public void process(RemoveItemFromCartCommand command) {

    }

    @CommandHandler
    public void process(LinkToAccountCommand command) {
        AggregateLifecycle.apply(new CartLinkedToAccountEvent(command.cartId, command.account));
        if (command.account.totalBalance < this.totalPrice) {

        }

    }

    @CommandHandler
    public void process(MergeCartForAccountCommand command) {

    }

    @CommandHandler
    public void process(ExpireCartCommand command) {

    }

    @CommandHandler
    public void process(CheckoutCartCommand command) {

    }

    @EventSourcingHandler
    public void on(CartCreatedEvent event) {
        this.cartId = event.cartId;
        this.expiryTime = event.cartExpiry;
    }

    @EventSourcingHandler
    public void on(LinkedCartCreatedEvent event) {

    }

    @EventSourcingHandler
    public void on(AddedItemToCartEvent event) {
        OrderItem existing = this.items.stream()
                .filter(i -> i.productId.equals(event.item.productId))
                .findFirst()
                .orElse(null);
        if (existing != null) {
            existing.quantity = existing.quantity.add(event.item.quantity);
            // we should update the price here, since the user is probably seeing the new price
            existing.price = event.item.price;
        } else {
            this.items.add(event.item);
        }
        this.totalPrice += event.item.quantity.quantity * event.productPrice;
        this.itemsCount = this.items.size();
    }

    @EventSourcingHandler
    public void on(ItemQuantityChangedEvent event) {

    }

    @EventSourcingHandler
    public void on(ItemRemovedFromCartEvent event) {

    }

    @EventSourcingHandler
    public void on(CartLinkedToAccountEvent event) {
        this.account = event.account;
    }


    @EventSourcingHandler
    public void on(CartMergedForAccountEvent event) {

    }


    @EventSourcingHandler
    public void on(CartExpiredEvent event) {

    }


    @EventSourcingHandler
    public void on(CartCheckoutEvent event) {

    }

    public ShoppingCartAggregate addItem(OrderItem item) {
        OrderItem existing = this.items.stream()
                .filter(i -> i.product.productId.equals(item.product.productId))
                .findFirst()
                .orElse(null);
        if (existing != null) {
            existing.quantity = existing.quantity.add(item.quantity);
            // we should update the price here, since the user is probably seeing the new price
            existing.price = item.price;
        } else {
            this.items.add(item);
        }
        this.totalPrice += item.quantity.quantity * item.product.price;
        this.itemsCount = this.items.size();
        return this;
    }

    public ShoppingCartAggregate changeItemQuantity(Long productId, Quantity newQuantity) {
        OrderItem existing = this.items.stream()
                .filter(i -> i.product.productId.equals(productId))
                .findFirst()
                .orElseThrow(NoProductInCartException::new);
        this.totalPrice -= existing.quantity.quantity * existing.product.price;
        existing.quantity = newQuantity;
        this.totalPrice += existing.quantity.quantity * existing.product.price;
        return this;
    }

    public ShoppingCartAggregate removeItem(Long productId) {
        OrderItem item = this.items.stream()
                .filter(i -> i.product.productId.equals(productId))
                .findFirst()
                .orElseThrow(NoProductInCartException::new);

        this.items.remove(item);
        this.totalPrice -= item.quantity.quantity * item.product.price;
        this.itemsCount = this.items.size();
        return this;
    }

    public ShoppingCartAggregate markAsOverfilled() {
        this.overfilled = true;
        return this;
    }

    public boolean hasLinkedAccount() {
        return this.account != null;
    }


    public boolean isExpired() {
        return LocalDateTime.now().isAfter(this.expiryTime);
    }
}
