package mk.ukim.finki.emt2019.ordermanagement.ports.kafka;

import mk.ukim.finki.emt2019.ordermanagement.model.Product;
import mk.ukim.finki.emt2019.ordermanagement.repository.jpa.ProductRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author Riste Stojanov
 */
@Profile("kafka")
@Component
public class ProductsSyncListener {

    private final ProductRepository productRepository;

    public ProductsSyncListener(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @KafkaListener(topics = "product-updates",
            groupId = "shopping-cart",
            containerFactory = "productsEventKafkaListenerFactory")
    public void updateProduct(Product product) {
        this.productRepository.save(product);
    }
}
