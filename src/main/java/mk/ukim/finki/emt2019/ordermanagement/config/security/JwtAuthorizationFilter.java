package mk.ukim.finki.emt2019.ordermanagement.config.security;

import io.jsonwebtoken.JwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Riste Stojanov
 */

@Component
public class JwtAuthorizationFilter implements Filter {
    public static final String AUTHORIZATION_HEADER = "Authorization";
    private static Logger logger = LoggerFactory.getLogger(JwtAuthorizationFilter.class);

    private final JwtHelper jwtHelper;

    public JwtAuthorizationFilter(JwtHelper jwtHelper) {
        this.jwtHelper = jwtHelper;
    }

    public Authentication getAuthentication(String token) {
        List grantedAuthorities = Stream.of(new SimpleGrantedAuthority("ROLE_USER"))
                .collect(Collectors.toList());

        return new PreAuthenticatedAuthenticationToken(this.jwtHelper.getProviderId(token), null, grantedAuthorities);
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            String token = httpServletRequest.getHeader(AUTHORIZATION_HEADER);
            if (token != null) {
                token = token.substring(6).trim();
                SecurityContextHolder
                        .getContext()
                        .setAuthentication(
                                this.getAuthentication(token)
                        );
            }
        } catch (JwtException var7) {
            logger.error(var7.getMessage(), var7);
        }
        chain.doFilter(request, response);
    }

    public void destroy() {
    }
}
