package mk.ukim.finki.emt2019.ordermanagement.model.projections;

import java.time.LocalDateTime;

/**
 * @author Riste Stojanov
 */
public interface ShoppingCartExpiryProjection {

    Long getCartId();

    LocalDateTime getExpiryTime();
}
