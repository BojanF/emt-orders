package mk.ukim.finki.emt2019.ordermanagement.domain.events;

/**
 * @author Riste Stojanov
 */
public class CartExpiredEvent {

    public String cartId;

    public CartExpiredEvent(String cartId) {
        this.cartId = cartId;

    }
}
