package mk.ukim.finki.emt2019.ordermanagement.repository.jpa;

import mk.ukim.finki.emt2019.ordermanagement.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Riste Stojanov
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
    ConcurrentHashMap getReservedQuantityForProduct(Long productId);
}
