package mk.ukim.finki.emt2019.ordermanagement.ports.listeners;

import mk.ukim.finki.emt2019.ordermanagement.model.events.CartCreatedEvent;
import mk.ukim.finki.emt2019.ordermanagement.model.events.CartExpiredEvent;
import mk.ukim.finki.emt2019.ordermanagement.model.exceptions.InvalidCartException;
import mk.ukim.finki.emt2019.ordermanagement.model.exceptions.InvalidExpiryTimeException;
import mk.ukim.finki.emt2019.ordermanagement.repository.mail.MailSenderRepository;
import mk.ukim.finki.emt2019.ordermanagement.service.CartManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import javax.mail.MessagingException;
import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Riste Stojanov
 */
@Profile("expire-on-event")
@Component
public class CartEventHandlers {

    private final Logger logger = LoggerFactory.getLogger(CartEventHandlers.class);

    private final ApplicationEventPublisher publisher;
    private final TaskScheduler scheduler;
    private final CartManagementService cartManagementService;
    private final MailSenderRepository mailSenderRepository;

    public CartEventHandlers(ApplicationEventPublisher publisher,
                             TaskScheduler scheduler,
                             CartManagementService cartManagementService, MailSenderRepository mailSenderRepository) {
        this.publisher = publisher;
        this.scheduler = scheduler;
        this.cartManagementService = cartManagementService;
        this.mailSenderRepository = mailSenderRepository;
    }

    @TransactionalEventListener
    public void onCartCreated(CartCreatedEvent event) {
        logger.info("Scheduling cart expired event for {}", event.getCart().cartId);
        this.scheduler.schedule(() -> {
            logger.info("Publishing cart expired event for {}", event.getCart().cartId);
            this.publisher.publishEvent(new CartExpiredEvent(event.getCart().cartId));
        }, Instant.from(event.getCart().expiryTime));

        this.scheduler.schedule(() -> {
            Map<String, String> map = new HashMap<>();
            map.put("person", "Riste Stojanov");
            map.put("time", "25.04.2019 14:00");
            try {
                this.mailSenderRepository.sendHtmlMail("cart owner",
                        "Cart is about to expire",
                        "cart-expiry-warning.mail.html",
                        map
                );
            } catch (MessagingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }, Instant.from(event.getCart().expiryTime.minusDays(1)));
    }

    @EventListener
    public void onCartExpired(CartExpiredEvent event) throws InvalidExpiryTimeException, InvalidCartException {
        this.cartManagementService.expireCart(event.getCartId());
    }
}
