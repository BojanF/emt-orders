package mk.ukim.finki.emt2019.ordermanagement.config.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import mk.ukim.finki.emt2019.ordermanagement.model.Quantity;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Riste Stojanov
 */
@Profile("kafka")
@EnableKafka
@Configuration
public class GenericKafkaConsumerConfig {


    @Value("${app.kafka.url}")
    private String bootstrapAddress;

    @Value("${app.kafka.group}")
    private String groupId;


    public <T> ConcurrentKafkaListenerContainerFactory<String, T> genericKafkaListenerFactory(Class<T> clazz) {

        ConcurrentKafkaListenerContainerFactory<String, T> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(genericConsumerFactory(clazz));
        return factory;
    }



    public  <T> ConsumerFactory<String, T> genericConsumerFactory(Class<T> clazz) {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                bootstrapAddress);
        props.put(ConsumerConfig.GROUP_ID_CONFIG,
                groupId);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                JsonDeserializer.class);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return new DefaultKafkaConsumerFactory<>(props,
                new StringDeserializer(),
                jsonDeserializer(clazz));
    }


    public  <T> Deserializer<T> jsonDeserializer(Class<T> clazz) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();

        SimpleModule module = new SimpleModule();
        // this is needed for the value objects
        module.addDeserializer(Quantity.class, new QuantityDeserializer());
        mapper.registerModule(module);

        JsonDeserializer<T> deserializer = new IgnoreTypeHeaderJsonDeserializer(clazz, mapper);
        deserializer.addTrustedPackages("*");
        return deserializer;
    }
}
