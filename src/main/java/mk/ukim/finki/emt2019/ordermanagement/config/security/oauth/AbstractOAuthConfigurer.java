package mk.ukim.finki.emt2019.ordermanagement.config.security.oauth;

import mk.ukim.finki.emt2019.ordermanagement.config.security.JwtHelper;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.UrlAuthorizationConfigurer;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import static mk.ukim.finki.emt2019.ordermanagement.config.security.SecurityUtils.ssoFilter;

public abstract class AbstractOAuthConfigurer extends AbstractHttpConfigurer<UrlAuthorizationConfigurer<HttpSecurity>, HttpSecurity> {

    private final OAuth2ClientContext oauth2ClientContext;
    protected final JwtHelper jwtHelper;

    public AbstractOAuthConfigurer(OAuth2ClientContext oauth2ClientContext, JwtHelper jwtHelper) {
        this.oauth2ClientContext = oauth2ClientContext;
        this.jwtHelper = jwtHelper;
    }

    @Override
    public void configure(HttpSecurity http) {
        http.addFilterBefore(
                ssoFilter(
                        oauth2ClientContext,
                        clientConfig(),
                        entryPointUrl(),
                        successHandler(),
                        failureHandler()),
                BasicAuthenticationFilter.class);
    }

    protected abstract AuthenticationSuccessHandler successHandler();

    protected abstract AuthenticationFailureHandler failureHandler();

    protected abstract ClientResources clientConfig();

    protected abstract String entryPointUrl();


}
