package mk.ukim.finki.emt2019.ordermanagement.repository.jpa.converters;

import mk.ukim.finki.emt2019.ordermanagement.model.Quantity;
import mk.ukim.finki.emt2019.ordermanagement.model.QuantityFactory;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Optional;

/**
 * @author Riste Stojanov
 */
@Converter(autoApply = true)
public class QuantityConverter implements AttributeConverter<Quantity, String> {
    @Override
    public String convertToDatabaseColumn(Quantity quantity) {
        return Optional.ofNullable(quantity)
                .map(Quantity::columnSerialize)
                .orElse(null);
    }

    @Override
    public Quantity convertToEntityAttribute(String column) {
        return Optional.ofNullable(column)
                .map(QuantityFactory::parse)
                .orElse(null);
    }
}