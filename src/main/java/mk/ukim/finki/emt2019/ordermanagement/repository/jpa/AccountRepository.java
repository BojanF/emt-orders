package mk.ukim.finki.emt2019.ordermanagement.repository.jpa;

import mk.ukim.finki.emt2019.ordermanagement.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account,Long> {

}
