package mk.ukim.finki.emt2019.ordermanagement.ports.scheduled;

import mk.ukim.finki.emt2019.ordermanagement.model.exceptions.InvalidCartException;
import mk.ukim.finki.emt2019.ordermanagement.model.exceptions.InvalidExpiryTimeException;
import mk.ukim.finki.emt2019.ordermanagement.model.projections.ShoppingCartExpiryProjection;
import mk.ukim.finki.emt2019.ordermanagement.repository.jpa.ShoppingCartRepository;
import mk.ukim.finki.emt2019.ordermanagement.service.CartManagementService;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Riste Stojanov
 */
@Profile("cron-invoked-expiry")
@Component
public class CleanExpiredCarts {

    private final ShoppingCartRepository shoppingCartRepository;

    private final CartManagementService cartManagementService;

    public CleanExpiredCarts(ShoppingCartRepository shoppingCartRepository, CartManagementService cartManagementService) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.cartManagementService = cartManagementService;
    }

    /**
     * Cron definition from https://www.baeldung.com/cron-expressions
     * <second> <minute> <hour> <day-of-month> <month> <day-of-week> <year>
     */
    @Scheduled(cron = "0 47 13 * * *")
    public void cleanExpiredCarts() {
        List<ShoppingCartExpiryProjection> expiredCarts = this.shoppingCartRepository.findByExpiryTimeAfter(LocalDateTime.now());
        expiredCarts
                .stream()
                .map(ShoppingCartExpiryProjection::getCartId)
                .forEach(cartId -> {
                    try {
                        this.cartManagementService.expireCart(cartId);
                    } catch (InvalidCartException e) {
                        e.printStackTrace();
                    } catch (InvalidExpiryTimeException e) {
                        e.printStackTrace();
                    }
                });
    }
}
