package mk.ukim.finki.emt2019.ordermanagement.domain.commands;

import mk.ukim.finki.emt2019.ordermanagement.model.Product;
import mk.ukim.finki.emt2019.ordermanagement.model.Quantity;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

/**
 * @author Riste Stojanov
 */
public class AddItemToNewCartCommand {

    @TargetAggregateIdentifier
    public String cartId;
    public Long cartExpiryInHours;
    public Product product;
    public Quantity quantity;

    public AddItemToNewCartCommand(String cartId, Long cartExpiryInHours, Product product, Quantity quantity) {
        this.cartId = cartId;
        this.cartExpiryInHours = cartExpiryInHours;
        this.product = product;
        this.quantity = quantity;
    }
}
