package mk.ukim.finki.emt2019.ordermanagement.domain.service;

import mk.ukim.finki.emt2019.ordermanagement.domain.commands.AddItemToNewCartCommand;
import mk.ukim.finki.emt2019.ordermanagement.model.*;
import mk.ukim.finki.emt2019.ordermanagement.model.exceptions.*;
import mk.ukim.finki.emt2019.ordermanagement.repository.jpa.AccountRepository;
import mk.ukim.finki.emt2019.ordermanagement.repository.jpa.ProductRepository;
import mk.ukim.finki.emt2019.ordermanagement.repository.jpa.ShoppingCartRepository;
import mk.ukim.finki.emt2019.ordermanagement.service.CartManagementService;
import mk.ukim.finki.emt2019.ordermanagement.service.ProductQuantityValidator;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;

import java.util.UUID;

/**
 * @author Riste Stojanov
 */
//@Transactional
//@Service
public class CartManagementCommandInvoker implements CartManagementService {

    private final ProductRepository productRepository;
    private final ProductQuantityValidator productQuantityValidator;
    @Deprecated
    private final ShoppingCartRepository shoppingCartRepository;
    private final AccountRepository accountRepository;
    private final CommandGateway commandGateway;
    private final ProductReservation productReservation;

    @Value("${app.cart_expiry_in_hours}")
    private Long cartExpiryInHours;

    public CartManagementCommandInvoker(ProductRepository productRepository,
                                        ProductQuantityValidator productQuantityValidator,
                                        ShoppingCartRepository shoppingCartRepository,
                                        AccountRepository accountRepository, ApplicationEventPublisher publisher,
                                        CommandGateway commandGateway, ProductReservation productReservation) {
        this.productRepository = productRepository;
        this.productQuantityValidator = productQuantityValidator;
        this.shoppingCartRepository = shoppingCartRepository;
        this.accountRepository = accountRepository;
        this.commandGateway = commandGateway;
        this.productReservation = productReservation;
    }

    @Override
    public ShoppingCart addItemToNewCart(Quantity quantity, Long productId)
            throws InvalidProductException, InvalidQuantityException, NotEnoughProductQuantityException {
        Product product = this.productRepository.findById(productId)
                .orElseThrow(InvalidProductException::new);

        checkProductAvailability(quantity, product);

        this.commandGateway.send(
                new AddItemToNewCartCommand(
                        UUID.randomUUID().toString(),
                        this.cartExpiryInHours,
                        product,
                        quantity
                )
        );


    }

    @Override
    public ShoppingCart addItemToNewLinkedCart(Long accountId, Quantity quantity, Long productId)
            throws InvalidAccountException, InvalidProductException, InvalidQuantityException, NotEnoughProductQuantityException {
        Product product = this.productRepository.findById(productId)
                .orElseThrow(InvalidProductException::new);
        Account account = this.accountRepository.findById(accountId)
                .orElseThrow(InvalidAccountException::new);

        ShoppingCart cart = createCartIfProductIsAvailable(quantity, product);
        cart.linkToAccount(account);
        if (account.totalBalance < cart.totalPrice) {
            cart.markAsOverfilled();
        }
//        this.eventPublisher.publishEvent(new CartCreatedEvent(cart));
        return this.shoppingCartRepository.save(cart);
    }

    @Override
    public ShoppingCart addItemToCart(Long cartId, Quantity quantity, Long productId)
            throws InvalidCartException, InvalidProductException, InvalidQuantityException, NotEnoughProductQuantityException {
        Product product = this.productRepository.findById(productId)
                .orElseThrow(InvalidProductException::new);
        ShoppingCart cart = this.shoppingCartRepository.findById(cartId)
                .orElseThrow(InvalidCartException::new);

        checkProductAvailability(quantity, product);

        OrderItem orderItem = OrderItem.createWithExpiryInHours(this.cartExpiryInHours, product, quantity);
        cart.addItem(orderItem);
        return this.shoppingCartRepository.save(cart);

    }

    @Override
    public ShoppingCart linkCartToAccount(Long cartId, Long accountId)
            throws InvalidCartException, InvalidAccountException, AccountLinkedToAnotherCartException {
        ShoppingCart cart = this.shoppingCartRepository.findById(cartId)
                .orElseThrow(InvalidCartException::new);
        Account account = accountRepository.findById(accountId)
                .orElseThrow(InvalidAccountException::new);
        ShoppingCart cartForAccount = this.shoppingCartRepository.findByAccount_AccountNumber(account.accountNumber);
        if (cartForAccount != null) {
            throw new AccountLinkedToAnotherCartException();
        } else {
            cart.linkToAccount(account);
            if (account.totalBalance < cart.totalPrice)
                cart.markAsOverfilled();
        }
        return this.shoppingCartRepository.save(cart);
    }

    @Override
    public ShoppingCart mergeCartForAccount(Long notLinkedCartId, Long accountId) throws InvalidCartException, InvalidAccountException {
        ShoppingCart notLinkedCart = this.shoppingCartRepository.findById(notLinkedCartId)
                .orElseThrow(InvalidCartException::new);
        Account account = this.accountRepository.findById(accountId)
                .orElseThrow(InvalidAccountException::new);
        ShoppingCart linkedCart = this.shoppingCartRepository.findByAccount_AccountNumber(account.accountNumber);
        if (linkedCart != null) {
            notLinkedCart.items.forEach(linkedCart::addItem);
            if (account.totalBalance < linkedCart.totalPrice) {
                linkedCart.markAsOverfilled();
            }
            this.shoppingCartRepository.deleteById(notLinkedCartId);
        }

        return this.shoppingCartRepository.save(linkedCart);
    }

    @Override
    public ShoppingCart markCartAsOverfilled(Long cartId) throws InvalidCartException {
        ShoppingCart cart = this.shoppingCartRepository.findById(cartId)
                .orElseThrow(InvalidCartException::new);
        cart.markAsOverfilled();
        return cart;
    }

    @Override
    public ShoppingCart changeItemQuantity(Long cartId, Quantity newQuantity, Long productId)
            throws InvalidCartException, InvalidProductException, InvalidQuantityException, NoProductInCartException,
            NotEnoughProductQuantityException {
        ShoppingCart cart = this.shoppingCartRepository.findById(cartId)
                .orElseThrow(InvalidCartException::new);

        Product product = this.productRepository.findById(productId)
                .orElseThrow(InvalidProductException::new);

        checkProductAvailability(newQuantity, product);

        cart.changeItemQuantity(productId, newQuantity);

        if (cart.hasLinkedAccount()) {
            if (cart.account.totalBalance < cart.totalPrice) {
                cart.markAsOverfilled();
            }
        }

        return this.shoppingCartRepository.save(cart);
    }

    @Override
    public ShoppingCart removeItemFromCart(Long cartId, Long productId)
            throws InvalidCartException, NoProductInCartException {

        ShoppingCart cart = this.shoppingCartRepository.findById(cartId)
                .orElseThrow(InvalidCartException::new);

        cart.removeItem(productId);

        return this.shoppingCartRepository.save(cart);
    }

    @Override
    public ShoppingCart expireCart(Long cartId) throws InvalidCartException, InvalidExpiryTimeException {
        ShoppingCart cart = this.shoppingCartRepository.findById(cartId)
                .orElseThrow(InvalidCartException::new);

        if (cart.isExpired()) {
            this.shoppingCartRepository.delete(cart);
        } else {
            throw new InvalidExpiryTimeException();
        }

        return cart;
    }

    @Override
    public ShoppingCart clearCart(Long cartId) throws InvalidCartException {
        return null;
    }

    @Override
    public ShoppingCart checkoutCart(Long orderId, Long accountId) throws InvalidCartException, InvalidAccountException {
        return null;
    }

    private ShoppingCart createCartIfProductIsAvailable(Quantity quantity, Product product) throws InvalidQuantityException, NotEnoughProductQuantityException {
        checkProductAvailability(quantity, product);

        ShoppingCart cart = ShoppingCart.createWithExpiryInHours(this.cartExpiryInHours);

        OrderItem orderItem = OrderItem.createWithExpiryInHours(this.cartExpiryInHours, product, quantity);
        cart.addItem(orderItem);
        return cart;
    }

    private void checkProductAvailability(Quantity quantity, Product product)
            throws InvalidQuantityException, NotEnoughProductQuantityException {
        if (!this.productQuantityValidator.isProductQuantityValid(quantity)) {
            throw new InvalidQuantityException();
        }

        Quantity reservedQuantity = this.productReservation.getReservedQuantityForProduct(product.productId);

        if (product.stockQuantity.subtract(reservedQuantity).isLessThan(quantity)) {
            throw new NotEnoughProductQuantityException();
        }
    }
}
