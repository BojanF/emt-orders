package mk.ukim.finki.emt2019.ordermanagement.config.security.oauth;

import mk.ukim.finki.emt2019.ordermanagement.config.security.JwtHelper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
@Configuration
@OAuthProvider
public class GoogleAuthentication extends AbstractOAuthConfigurer {

    public GoogleAuthentication(@Qualifier("oauth2ClientContext") OAuth2ClientContext oauth2ClientContext,
                                JwtHelper jwtHelper) {
        super(oauth2ClientContext, jwtHelper);
    }


    @Bean(name = "googleSuccessHandler")
    @Override
    protected AuthenticationSuccessHandler successHandler() {
        return new SignInSuccessHandler("GOOGLE", this.jwtHelper);
    }

    @Bean(name = "googleFailureHandler")
    @Override
    protected AuthenticationFailureHandler failureHandler() {
        return new SignInFailureHandler();
    }

    @Bean(name = "googleConfig")
    @ConfigurationProperties("google")
    public ClientResources clientConfig() {
        return new ClientResources();
    }

    @Override
    protected String entryPointUrl() {
        return "/login/google";
    }
}
