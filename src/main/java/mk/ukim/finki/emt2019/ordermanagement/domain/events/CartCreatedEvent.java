package mk.ukim.finki.emt2019.ordermanagement.domain.events;

import mk.ukim.finki.emt2019.ordermanagement.model.Product;
import mk.ukim.finki.emt2019.ordermanagement.model.Quantity;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.time.LocalDateTime;

/**
 * @author Riste Stojanov
 */
public class CartCreatedEvent {


    @TargetAggregateIdentifier
    public String cartId;

    public LocalDateTime cartExpiry;

    public Product product;

    public Quantity quantity;

    public CartCreatedEvent(String cartId, LocalDateTime cartExpiry, Product product, Quantity quantity) {
        this.cartId = cartId;
        this.cartExpiry = cartExpiry;
        this.product = product;
        this.quantity = quantity;
    }
}
