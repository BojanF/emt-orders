package mk.ukim.finki.emt2019.ordermanagement.config.security;

import mk.ukim.finki.emt2019.ordermanagement.config.security.oauth.ClientResources;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.Filter;

/**
 * @author Riste Stojanov
 */
public class SecurityUtils {

    public static Filter ssoFilter(
            OAuth2ClientContext oauth2ClientContext,
            ClientResources client,
            String path,
            AuthenticationSuccessHandler successHandler,
            AuthenticationFailureHandler failureHandler) {
        OAuth2ClientAuthenticationProcessingFilter oAuth2ClientAuthenticationFilter =
                new OAuth2ClientAuthenticationProcessingFilter(path);
        OAuth2RestTemplate oAuth2RestTemplate =
                new OAuth2RestTemplate(
                        client.getClient(),
                        oauth2ClientContext
                );
        oAuth2ClientAuthenticationFilter.setRestTemplate(oAuth2RestTemplate);
        UserInfoTokenServices tokenServices = new UserInfoTokenServices(
                client.getResource().getUserInfoUri(),
                client.getClient().getClientId());
        tokenServices.setRestTemplate(oAuth2RestTemplate);
        oAuth2ClientAuthenticationFilter.setTokenServices(tokenServices);
        oAuth2ClientAuthenticationFilter.setAuthenticationSuccessHandler(successHandler);
        oAuth2ClientAuthenticationFilter.setAuthenticationFailureHandler(failureHandler);
        return oAuth2ClientAuthenticationFilter;
    }
}
