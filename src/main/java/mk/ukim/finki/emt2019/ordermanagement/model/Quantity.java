package mk.ukim.finki.emt2019.ordermanagement.model;

import mk.ukim.finki.emt2019.ordermanagement.model.exceptions.UnComparableQuantitiesException;

/**
 * @author Riste Stojanov
 */
public class Quantity implements Comparable<Quantity> {

    public final Double quantity;

    public final Unit unit;

    public Quantity(double quantity, Unit unit) {
        if (unit == null) {
            throw new IllegalArgumentException("Unit can not be null");
        }
        this.quantity = quantity;
        this.unit = unit;
    }

    @Override
    public int hashCode() {
        int result = quantity.hashCode();
        result = 31 * result + unit.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Quantity quantity1 = (Quantity) o;

        if (!quantity.equals(quantity1.quantity)) return false;
        return unit == quantity1.unit;
    }

    @Override
    public int compareTo(Quantity o) {
        if (!o.unit.equals(this.unit)) {
            throw new UnComparableQuantitiesException();
        }
        return this.quantity.compareTo(o.quantity);
    }

    public Quantity subtract(Quantity value) {
        if (!value.unit.equals(this.unit)) {
            throw new UnComparableQuantitiesException();
        }
        return new Quantity(this.quantity - value.quantity, this.unit);
    }

    public boolean isLessThan(Quantity quantity) {
        return this.compareTo(quantity) < 0;
    }

    public Quantity add(Quantity value) {
        if (value == null) {
            return this;
        }
        if (!value.unit.equals(this.unit)) {
            throw new UnComparableQuantitiesException();
        }
        return new Quantity(this.quantity + value.quantity, this.unit);
    }

    public String columnSerialize() {
        return this.quantity + "|" + this.unit.toString();
    }
}
