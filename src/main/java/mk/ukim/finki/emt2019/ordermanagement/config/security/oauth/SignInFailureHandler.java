package mk.ukim.finki.emt2019.ordermanagement.config.security.oauth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SignInFailureHandler implements AuthenticationFailureHandler {

    @Value("${app.client_url}")
    public String clientAppUrl;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        try {
            System.out.println("In failure! Params: ");
            System.out.println(request.getParameterMap());
        } finally {
            response.sendRedirect(clientAppUrl + "/oauth/failed");
        }
    }
}
