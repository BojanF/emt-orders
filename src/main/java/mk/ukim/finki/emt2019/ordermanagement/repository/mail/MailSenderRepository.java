package mk.ukim.finki.emt2019.ordermanagement.repository.mail;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Map;

/**
 * @author Riste Stojanov
 */
public interface MailSenderRepository {

    void sendHtmlMail(String to,
                      String subject,
                      String template,
                      Map<String, String> params) throws MessagingException, IOException;
}
