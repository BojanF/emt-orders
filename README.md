# Order management sub-domain

This project aims to show the use of Domain Driven Design (DDD) in a given sub-domain. It is not intended to identify 
multiple Bounded-contexts and their mapping. Its goal is to show how we can design a single module with defined responsibility.

The initial use cases covered by this project are given in the following [Event Storming notes](https://realtimeboard.com/app/embed/o9J_kxhhdAU=/). 

In short, we are aiming to develop a module that will handle the shopping cart management and displaying in a pre-paid 
system. The processes planned to be covered are the following:
 
- Adding an item to the shopping cart for anonymous and registered user, where the first item will initialize the shopping cart
    - There will be one shopping cart associated to each registered user
    - The shopping cart will calculate the total amount of the added items 
- Modifying and removing the items from the shopping cart
- Checkout the shopping cart
    - In this process, the pre-paid balance must be validated and updated, as well as the stock quantity of the products.  

## Lecture 1

In this lecture we are identifying the Domain model and its behavior. For the domain model, we have identified the following 
classes: 

- `Account`
    - This is an **Aggregate root entity** that will be obtained from other system and will represent the available balance 
    of a given registered client. 
- `Product`
    - is an **Aggregate root entity** that will contain the price and the available stock quantity of the products. This 
    entity is managed by the inventory system. 
- `ShoppingCart`
    - The shopping cart is an **Aggregate root entity** that will compute the total amount based on the items added into it. 
    - It will validate the product availability in the moment of their insertion into the cart. 
    - It may be `overfilled` if its total amount is more that the account's balance.
    - It contains the `OrderItem`s that the customer has added in his/hers cart
- `OrderItem`
    - is an **Entity** that belongs to the `ShoppingCart` aggregate.
    - it contains a copy of the `Product` price at the moment of its creation, the desired quantity and a reference to 
    the product itself
- `Quantity`
    - is a **Value Object** that represents a certain quantity of a given `Unit`
- `Unit`
    - is an enumeration that provides the available units in the system

The behaviour is initially designed in the `CartManagementService`. Here we design the interface methods with names
that aim to describe the `Method Intention` i.e., we are trying to make the **Intention Revealing Interfaces**. These
methods define the input parameters of each intention, the result (the returning value) and the invalid scenarios with 
the defined Exceptions.       


## Lecture 2

In this lecture we start implementing the interfaces. During this implementation, we add extend our Domain objects 
with additional methods. Such methods in the `ShoppingCart` aggregate are: 

- `addItem`
- `removeItem`
- `changeItemQuantity`
- `markAsOverfilled`
- `linkToAccount`
- `hasLinkedAccount`

Note that all these methods are named based on the business case they are covering. The methods added in the *Domain Object* 
should only manage its state. They should not contain business related logic. Therefore, the method `addItem` is not contain
a suitable implementation since we may change the way we are combining the multiple `OrderItems` for same `Product`. In 
the current implementation, we are updating the old `price` of the `OrderItem` with the price of the `Product` in the new 
moment, which is a business decision that may change. Therefore, we should consider refactoring this method in the later
lectures.


In this lecture we also implement most of the methods defined in the `CartManagementService`. During the implementation, 
we decide that the `OrderItem` should also have an expiry time, in order to prevent reservation of products that are 
on high demand for a long period of time. However, in the the current version, all items will have the expiration time
of the shopping cart. 

The `Quantity` is a **Value Object** which should be immutable and identified solely on its attributes. Therefore, we 
implement the corresponding methods for its manipulation, such as `add`, `subtract` and `isLessThan` that ensure its immutability, 
as well as its `compareTo`, `equals` and `hashCode` for sorting and identifying it everywhere in the Java Virtual Machine (JVM). 


## Lecture 3 

During this lecture we are working on persisting the state of our *Domain Objects*. 

In order to store the `LocalDateTime` and the `Quantity` value objects, we are using the `LocalDateTimeConvertor` and 
`QuantityConvertor`, which implement the JPA's `AttributeConverter` interface. This way we define a way to serialize 
these value objects via types recognized by the database engines. 

    

## Testing with Docker

A `docker-compose` file is provided in the root directory of the project. At any time, a Docker image of the project can be built by running the following commands:

- `docker build -t emt .`
- `docker-compose up -d` (the `-d` switch can be ommited if you want your console to become attached to stdout and stderr so that you can follow the application logs)
- You can bring down the application with `docker-compose down` or restart it with `docker-compose restart`.
- If you brought up the application with the `-d` switch, you can attach your console to stderr and stdout using `docker-compose logs -f`

The `docker-compose` file can be further customized so that only the database server runs, or the application. Comments have been added so that the various options are clearly specified.