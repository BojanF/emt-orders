FROM maven:3-jdk-8-alpine as builder
COPY . /tmp
WORKDIR /tmp
RUN mvn clean install -DskipTests

FROM openjdk:8-jdk-alpine
RUN mkdir /order_management
COPY --from=builder /tmp/target/order-management-0.0.1-SNAPSHOT.jar /order_management/order_management.jar
RUN addgroup -S emt && adduser -S emt -G emt
RUN apk add --no-cache bash
ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh /order_management/init.sh
RUN chown -R emt:emt /order_management
USER emt
WORKDIR /order_management
RUN chmod u+x /order_management/init.sh
RUN ls
ENTRYPOINT ["./init.sh", "mariadb:3306", "--", "java","-Djava.security.egd=file:/dev/./urandom","-jar","order_management.jar"]
